package ictgradschool.industry.abstraction.farmmanager.animals;

/**
 * This class defines a Cow. A cow has an initial value of $1000, and a maximum value of $1500.
 * The cost to feed a cow is $50.
 * You may modify this class according to the assignment.
 *
 * @author write your name and UPI here.
 */
public class Sheep extends Animal {
    private final int MAX_VALUE = 1000;

    public Sheep() {
        value = 250;
    }

    public void feed() {
        if (value < MAX_VALUE) {
            value = value + (MAX_VALUE - value) / 3;

        }
    }

    public int costToFeed() {
        return 6;
    }

    public String getType() {
        return "Sheep";
    }

    public String toString() {
        return getType() + " - $" + value;
    }
}
